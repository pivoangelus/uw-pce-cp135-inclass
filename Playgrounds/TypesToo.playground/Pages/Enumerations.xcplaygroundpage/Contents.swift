//: [Previous](@previous)

enum Tutorial: String {
    case LLDB = "Debugging in LLDB"
    case iOS9  = "hello"
    case Swift
    case CoreData
}

var lldbTutorial = Tutorial.LLDB

var ios9Stuff = Tutorial.iOS9
var coredataTutorial = Tutorial.CoreData

ios9Stuff = .Swift

ios9Stuff.rawValue
lldbTutorial.rawValue
coredataTutorial.rawValue

if let validTutorial = Tutorial(rawValue: "hello") {
    print ("valid")
    print (validTutorial)
}
else {
    print ("nope")
}


//: [Next](@next)
