class ConferenceAttendee {
    let name : String
    var hometown : String?
    var isFromSomewhere: Bool {
        get {
            return hometown != nil
        }
        set (newValue) {
            if newValue {
                hometown = "Carnation"
            }
            else {
                hometown = nil
            }
        }
    }
    
    init () {
        name = "Daniel"
        self.hometown = nil
    }
    
    init (name: String, hometown: String? = nil) {
        self.name = name
        self.hometown = hometown
    }
    
    func description () -> String {
        if let hometown = self.hometown {
            return self.name + hometown
        }
        return self.name
    }
}

let daniel = ConferenceAttendee()

let hal = ConferenceAttendee(name: "Hal", hometown: "Texas")
hal.name
hal.description()
daniel.description()
let marty = ConferenceAttendee(name: "Marty")

marty.hometown
marty.description()

hal.isFromSomewhere
marty.isFromSomewhere
marty.isFromSomewhere = true

marty.description()

class TutorialAttendee: ConferenceAttendee {
    let tutorial: String
    
    init(name: String, tutorial: String, hometown: String? = nil) {
        self.tutorial = tutorial
        super.init(name: name, hometown: hometown)
    }
}

let xyzzy = TutorialAttendee(name: "xyzzy", tutorial: "Core Data")
xyzzy.hometown

