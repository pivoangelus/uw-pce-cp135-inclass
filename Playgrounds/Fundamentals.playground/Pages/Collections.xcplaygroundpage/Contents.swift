//: [Previous](@previous)

var numbers = [0, 1, 2, 3]

let one = numbers[1]
print(one)

numbers[1] = 3

print(numbers[1])

/* NSArray *numbers = [NSArray arrayWithObjects:
[NSNumber numberWithInt:1],
[NSNumber numberWithInt:2],
[NSNumber numberWithInt:3],
nil];

@[[NSNumber numberWithInt:1],
[NSNumber numberWithInt:2],
[NSNumber numberWithInt:3]];

@[@1, @2, @3]
*/

import Foundation

let badNumbers = [0, 1, 2, 4]

print (badNumbers[3])

let badOne = (badNumbers[1] as! NSNumber).integerValue
let badTwo = (badNumbers[2] as! NSNumber).integerValue

let badCheeseOptional = badNumbers[3] as? NSNumber
print(badCheeseOptional)
if let badCheese = badNumbers[3] as? NSNumber {
    print(badOne + badCheese.integerValue)
}

let sum = badOne + badTwo

var digits = ["one": 1,
    "two": 2,
    "three": 3,
    "five": 555]

digits["two"]
digits.removeValueForKey("six")

for key in digits.keys {
    print(digits[key])
}

print ("----")
let threeValue = digits["three"]
print(threeValue)

if let fiveValue = digits["five"] {
    print(fiveValue)
}
else {
    print ("nobody home")
}



