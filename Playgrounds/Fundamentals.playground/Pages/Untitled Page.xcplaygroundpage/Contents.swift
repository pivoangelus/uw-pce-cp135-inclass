//: ## Functions

//: [TOC](TOC) - [Previous](@previous) - [Next](@next)

let string = "Hello Playground"

print(string)

func hello () {
    print ("hello world")
}

hello()

func hello (name: String) {
    print ("hello", name)
}

hello("everyone")

func hello (name: String, numberOfTimes: Int) {
    for _ in 1 ... numberOfTimes {
        hello(name)
    }
}

hello("new Mariners manager", numberOfTimes: 12)

/*
(void) helloWithName:(NSString *)name

(void) helloWithName:(NSString *)name numberOfTimes: (NSInteger) count

*/

//: Don't omit parameter names, even though you can
func zhello (outsideName name: String, _ numberOfTimes: Int, _ color: String) {
    for _ in 1 ... numberOfTimes {
        hello(name)
        print(color)
    }
}


zhello(outsideName: "Lou", 12, "red")

func hey (name: String = "world", numberOfTimes: Int = 1) {
    for _ in 1 ... numberOfTimes {
        hello(name)
    }
}



print("hello\(string)")
print("hello", string)
print("hello", string, separator:"")
print("hello", string, separator:"anything")

func hi(name: String = "World") {
    hello(name)
}

hi()
hi("Marty")

hey()
hey("something else")
hey("something cool", numberOfTimes: 4)
hey(numberOfTimes: 3)
print("----")

func welcome(people: String...) {
    for person in people {
        hello(person)
    }
}

welcome()

welcome("Larry", "Moe", "Curly")

let cities = ["Bellevue", "Seattle", "Redmond"]

//let things = ["car", "hammer", 3]

//welcome(cities)

func whatsUp(name: String) -> String {
//    return "Hello \(name)"
    return "Hello " + name
}

let captain = "Sisko"

let greeting = whatsUp(captain)

func helloFriends(people: String...) -> (numberofFriends: Int, greeting: String) {
    var tempGreeting = "Howdy "
    for person in people {
        tempGreeting += person + " "
    }
    return (people.count, tempGreeting)
}

let howdy = helloFriends("Larry", "Moe", "Curley")

print(howdy)
print(howdy.0)
print(howdy.1)
print(howdy.numberofFriends)
print(howdy.greeting)





