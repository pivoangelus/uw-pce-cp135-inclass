var number = 6

func mutatingDouble(inout input: Int) {
    input = input * 2
}

mutatingDouble(&number)

number

func double (input: Int) -> Int {
    return input * 2
}

number = 6

double(number)
number

number = double(number)
number
