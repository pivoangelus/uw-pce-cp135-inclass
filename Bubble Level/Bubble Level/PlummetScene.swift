//
//  PlummetScene.swift
//  Bubble Level
//
//  Created by Hal Mueller on 10/22/15.
//  Copyright © 2015 Hal Mueller. All rights reserved.
//

import SpriteKit
import CoreMotion

class PlummetScene: SKScene {
    var markerSprite: SKShapeNode!
    var centerPoint: SKShapeNode!
    
    var motionManager: CMMotionManager!
    
    func radius() -> CGFloat {
        return self.frame.size.width * 0.4
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(size: CGSize) {
        super.init(size: size)
        
        motionManager = CMMotionManager()
        motionManager.startDeviceMotionUpdates()
//        motionManager.startDeviceMotionUpdatesUsingReferenceFrame(.XTrueNorthZVertical)
        
        print(motionManager.deviceMotionAvailable)
        
        let outsideCircle = SKShapeNode(circleOfRadius: self.radius())
        outsideCircle.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        outsideCircle.strokeColor = SKColor.blackColor()
        outsideCircle.lineWidth = 4
        self.addChild(outsideCircle)
        
        centerPoint = SKShapeNode(circleOfRadius: 2.0)
        centerPoint.position = outsideCircle.position
        centerPoint.fillColor = SKColor.blueColor()
        
        markerSprite = SKShapeNode(circleOfRadius: 5.0)
        markerSprite.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame) + 50)
        markerSprite.fillColor = SKColor.redColor()
        
        self.addChild(markerSprite)
        self.addChild(centerPoint)
    }
    
    override func update (currentTime: NSTimeInterval) {
        if let motion = motionManager.deviceMotion {
            let gravity = motion.gravity
            print("gravity:", gravity.x, gravity.y, gravity.z)
            
            let gravityMultiple = -150.0
            let xDelta = CGFloat(gravity.x * gravityMultiple)
            let yDelta = CGFloat(gravity.y * gravityMultiple)
            markerSprite.position = CGPointMake(centerPoint.position.x + xDelta, centerPoint.position.y + yDelta)
        }
    }
}